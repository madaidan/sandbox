# Sandbox

A simple script to create a restrictive bubblewrap sandbox.

It creates an entire new filesystem, mounts a few important files and directories, creates a PID, cgroup and UTS namespace, sets the hostname to "host" and drops all capabilities.

It uses Xephyr for an X11 sandbox and sets it's resolution to 1400x1050.

Some applications may not work with this.

# Usage

Run it with the name of the program you want to sandbox after the command. For example, to sandbox Firefox, run

    sandbox firefox
    
You can also modify the sandbox with some parameters as the parameters are given directly to bubblewrap. For example, to sandbox Firefox and create a network namespace to run it without any network access, run

    sandbox --unshare-net firefox
    
You can add whatever parameter you want as long as it works with bubblewrap.

# Dependencies

[bubblewrap](https://github.com/projectatomic/bubblewrap) 0.3.3 <br>
Xephyr